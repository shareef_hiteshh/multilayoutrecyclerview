package com.android.twolayoutrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private static final String TAG = RecyclerAdapter.class.getSimpleName();

    private static final int LIST_ITEM_TYPE_1 = 1;
    private static final int LIST_ITEM_TYPE_2 = 2;

    private static final int ITEM_COUNT = 40;

    private List<Model> mData;
    private LayoutInflater mInflater;

    private int mSelectedPosition;

    RecyclerAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        mData = new ArrayList<>(ITEM_COUNT);
        for (int i = 1; i <= ITEM_COUNT; ++i) {
            Model model = new Model();
            model.phoneNumber = "" + i;
            mData.add(model);
        }
        mSelectedPosition = -1;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder, viewType = " + viewType);
        View view;
        if (viewType == LIST_ITEM_TYPE_1) {
            view = mInflater.inflate(R.layout.list_item_1, parent, false);
        } else {
            view = mInflater.inflate(R.layout.list_item_2, parent, false);
        }
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder, position = " + position);
        Model model = holder.mModel = mData.get(position);
        if (position == mSelectedPosition) {
            holder.mEdittext.setText(model.phoneNumber);
        } else {
            holder.mPhoneNumberView.setText(model.phoneNumber);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position == mSelectedPosition ? LIST_ITEM_TYPE_1 : LIST_ITEM_TYPE_2;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Model mModel;
        EditText mEdittext;
        TextView mPhoneNumberView;
        Button mDoneButton, mCancelButton;
        Button mEditButton, mDeleteButton;
        LinearLayout mMainLayout;

        RecyclerViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mEdittext = (EditText) itemView.findViewById(R.id.selected_number);
            mPhoneNumberView = (TextView) itemView.findViewById(R.id.phone_number);
            mDoneButton = (Button) itemView.findViewById(R.id.done);
            mCancelButton = (Button) itemView.findViewById(R.id.cancel);
            mEditButton = (Button) itemView.findViewById(R.id.edit);
            mDeleteButton = (Button) itemView.findViewById(R.id.delete);
            mMainLayout = (LinearLayout) itemView.findViewById(R.id.main_layout);

            if (mDoneButton != null)
                mDoneButton.setOnClickListener(this);
            if (mCancelButton != null)
                mCancelButton.setOnClickListener(this);
            if (mDeleteButton != null)
                mDeleteButton.setOnClickListener(this);
            if (mEditButton != null)
                mEditButton.setOnClickListener(this);
            if (mMainLayout != null)
                mMainLayout.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick, getAdapterPosition = " + getAdapterPosition());
            switch (v.getId()) {
                case R.id.done:
                    mModel.phoneNumber = mEdittext.getText().toString();
                    mSelectedPosition = -1;
                    notifyItemChanged(getAdapterPosition());
                    break;

                case R.id.cancel:
                    mSelectedPosition = -1;
                    notifyItemChanged(getAdapterPosition());
                    break;

                case R.id.delete:
                    mSelectedPosition = -1;
                    mData.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    break;

                case R.id.main_layout:
                case R.id.edit:
                    mSelectedPosition = getAdapterPosition();
                    notifyItemChanged(getAdapterPosition());
                    break;
            }
        }
    }
}
